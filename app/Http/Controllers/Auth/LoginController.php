<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }


    public function login(Request $request){
        /*$this->validate($request, [
            'name' => 'required',
            ]);
        if (\Auth::attempt([
            'name' => $request->name])
        ){
            return redirect('/dashboard');
        }
        return redirect('/login')->with('error', 'Nama dan Nomor Ujian Tidak ditemukan');*/


        $credentials = array(
            'name' => $request->name,
            'test_no' =>$request->test_no,
        );

        if(Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            return redirect('/login')->with('error', 'Nama dan Nomor Ujian Tidak ditemukan');
        }


    }

    // *
    //  * Obtain the user information from GitHub.
    //  *
    //  * @return \Illuminate\Http\Response
     
    public function handleProviderCallback($service)
    {
        $userSocial = Socialite::driver($service)->user();

        //return $userSocial->name;
        $findUser=User::where('email',$userSocial->email)->first();
        if($findUser)
        {
             Auth::login($findUser);
                return '<script>if (window.opener) {window.opener.location.href="/quickquiz/public"; window.close();}</script>';

        }
        else
        {
        $user = new User;
        $user->name = $userSocial->name;
        $user->email = $userSocial->email;
        $user->password = Hash::make(123456);
        $user->role = "S";
        $user->save();
        $this->guard()->login($user);

       
        return '<script>if (window.opener) {window.opener.location.href="/quickquiz/public"; window.close();}</script>';
        }
        
    }
}
  
